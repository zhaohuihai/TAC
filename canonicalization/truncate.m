function [U, S, V, truncationError] = truncate(U, S, V, Dcut)


if Dcut < length(S)
    truncationError = 1 - sum(S(1 : Dcut).^2) / sum(S.^2) ;
    
    S = S(1 : Dcut) ;
    U = U(:, 1 : Dcut) ;
    V = V(:, 1 : Dcut) ;
else
    truncationError = 0 ;
end


