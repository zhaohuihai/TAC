function verifyCano_single(A, S)
%* A(a,i,b)

S = diag(S) ;

%* AS(a1,m,b2) = sum{b1}_[A(a1,m,b1) * S(b1,b2)]
AS = contractTensors(A, 3, 3, S, 2, 1) ;
%* ASA(a1,a2) = sum{m,b2}_[AS(a1,m,b2) * A(a2,m,b2)]
ASA = contractTensors(AS, 3, [2, 3], A, 3, [2, 3]) ;
eta = S(1,1) / ASA(1, 1) ; 
cond1 = norm(ASA * eta - S) 
%=========================================================================
%* SA(a1,m,b2) = sum{a2}_[S(a1,a2)*A(a2,m,b2)]
SA = contractTensors(S, 2, 2, A, 3, 1) ;

%* ASA(b1,b2) = sum{a1,m}_[A(a1,m,b1) * SA(a1,m,b2)]
ASA = contractTensors(A, 3, [1, 2], SA, 3, [1, 2]) ;

eta = S(1,1) / ASA(1, 1) ; 
cond2 = norm(ASA * eta - S) 

