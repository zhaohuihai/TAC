function L = computeLeftResidual(A, varargin)
% function L = computeLeftResidual(A)
% function L = computeLeftResidual(A, L)
%* L(b,b2)

[da, di, db] = size(A) ;

if nargin == 2
    L = varargin{1} ;
    %* A(a,i,b) = sum{b1}_[A(a,i,b1)*L(b1,b)]
    A = contractTensors(A, 3, 3, L, 2, 1) ;
end

%* A(a,i,b) -> A(a,(i,b))
A = reshape(A, [da, di * db]) ;

%* A(a,(i,b)) = sum{a1}_[L(a,a1)*Q(a1,(i,b))]
L = lq(A) ;

L = L ./ norm(L) ;