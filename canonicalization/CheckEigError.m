function [ EigdecomError, D1 ] = CheckEigError ( U1, D1, X )

d1 = length ( X ) ;
Maxdiff = @ ( x, y ) max ( max ( abs ( x - y ) ) ) ;

ud1 = U1 * D1 * U1' ;
cmp1 = eye ( d1 ) ;
UnitaryError = max ( Maxdiff ( U1 * U1', cmp1 ), Maxdiff ( U1' * U1, cmp1 ) ) ;
DecomError = Maxdiff ( ud1, X ) ;
NormalizeError = abs ( trace ( D1 ) - 1 ) ;

EigdecomError.DecomError = DecomError ;
EigdecomError.UnitaryError = UnitaryError ;
EigdecomError.NormalizeError = NormalizeError ;

D1 = D1 / trace ( D1 ) ;